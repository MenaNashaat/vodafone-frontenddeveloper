import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from '../_pages/home/home.component';

const routes: Routes = [
  {
      path: '',
      component: HomeComponent,
  },
  {
    path: 'home',
    component: HomeComponent,
},
  { path: 'addContact', 
     loadChildren: () => import('../_pages/add-contact/add-contact.module').then(m => m.AddContactModule)
},
  { path: 'editContact/:id', 
     loadChildren: () => import('../_pages/edit-contact/edit-contact.module').then(m => m.EditContactModule)
} ,
{ path: 'profile/:id', 
     loadChildren: () => import('../_pages/profile/profile.module').then(m => m.ProfileModule)
} 
];

@NgModule({
  imports: [
      RouterModule.forRoot(routes)
  ],
  exports: [
      RouterModule
  ],
  declarations: []
})
export class AppRoutingModule { }
