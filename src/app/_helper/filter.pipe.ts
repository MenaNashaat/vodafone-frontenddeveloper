import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

  transform(items: any[], searchText: string): any[] {
    if(!items) return [];

    if(!searchText) return items;

    return this.searchItems(items, searchText.toLowerCase());
   }

   private searchItems(items :any[], searchText): any[] {
     let results = [];
     console.log("items"+JSON.stringify(items))
      items.forEach(it => {
         console.log("it"+JSON.stringify(it.title.toLowerCase()))
         let n = searchText.length;
          console.log("n"+n)
         let res = it.title.toLowerCase().slice(0, n);
          console.log("res"+res)
           if (res== searchText)
           {
               console.log("yes")
             results.push(it);
           }
        // if (it.title.toLowerCase().indexOf(searchText) !== -1) {
        //     results.push(it);
        // }
      });
      return results;
   }
}