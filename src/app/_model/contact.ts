export class Contact {
    
    id: number;
    title: string;
    email: string;
    phone: number;
  
}