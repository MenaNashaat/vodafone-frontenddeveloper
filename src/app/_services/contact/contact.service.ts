import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class ContactService {

  constructor(private http:HttpClient) { }
  getContacts(): Observable<any> {
    return this.http.get<any>(`${environment.apiUrl}/people`  );
  }
  addContacts(contact): Observable<any> {
    return this.http.post<any>(`${environment.apiUrl}/people` , contact );
  }
  getContactById(id): Observable<any> {
    return this.http.get<any>(`${environment.apiUrl}/people/${id}`  );
  }
  deletetContact(id): Observable<any> {
    return this.http.delete<any>(`${environment.apiUrl}/people/${id}`  );
  }
  editContact(id, contact): Observable<any> {
    return this.http.put<any>(`${environment.apiUrl}/people/${id}`, contact  );
  }
}
