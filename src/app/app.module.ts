import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing/app-routing.module';

import { AppComponent } from './app.component';
import { HomeComponent } from './_pages/home/home.component';
import { FooterComponent } from './_pages/footer/footer.component';
import { HeaderComponent } from './_pages/header/header.component';
import { ToastsComponent } from './_pages/toasts/toasts.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { FilterPipe } from './_helper/filter.pipe'
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms'; 
import { ReactiveFormsModule } from '@angular/forms';
@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    FooterComponent,
    HeaderComponent,
    ToastsComponent, 
    FilterPipe,
    
  ],
  imports: [
    BrowserModule,
    NgbModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
