import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import {ToastService , ContactService} from '../../_services'
import { Contact} from '../../_model/contact'
@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  loading = false;
  submitted = false;
  returnUrl: string;
  details :Contact;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private toastService: ToastService,
    private contactService:ContactService
  ) { }

  ngOnInit(): void {
    this.getDetails()
  }
  showSuccess() {
    this.toastService.show('Delete Contact successful', { classname: 'bg-success text-light', delay: 3000 });
  }

  showDanger(test) {
    this.toastService.show(test , { classname: 'bg-danger text-light', delay: 3000 });
  }
  getDetails()
  {
    const id = this.route.snapshot.paramMap.get('id');
    this.contactService.getContactById(id).subscribe(
      data => {
        this.details = data;
        console.log("this.details"+ JSON.stringify(this.details))
      },
      error => {
        this.showDanger(error)
        this.loading = false;
      });
  }
  deleteContact(id){
    this.contactService.deletetContact(id).subscribe(
      data => {
        this.router.navigate(['/home']);
        this.showSuccess()
      },
      error => {
        this.showDanger(error)
        this.loading = false;
      });
  }

}
