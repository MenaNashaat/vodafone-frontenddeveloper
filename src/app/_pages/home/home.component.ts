import { Component, OnInit } from '@angular/core';
import {ContactService} from '../../_services';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  items:any;
  searchText = '';
  characters:any = [];
  constructor(
    private contactService:ContactService
  ) { }

  ngOnInit(): void {
    this.getAllContacts()
  }
 getAllContacts()
 {
 this.contactService.getContacts().subscribe(data => {this.items = data;
console.log("this.items" +JSON.stringify(this.items))
for (var i = 0; i < this.items.length; i += 1) {
  // Use the index i here
  console.log(this.items[i].title.charAt(0));
  if (!this.characters.includes(this.items[i].title.charAt(0)))
  {
    this.characters.push(this.items[i].title.charAt(0))
  }
}
console.log("this.characters" +JSON.stringify(this.characters))

})

 }
 selectCharacter(char)
 {
   this.searchText =char
 }
}
