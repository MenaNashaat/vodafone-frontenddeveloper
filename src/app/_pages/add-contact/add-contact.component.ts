import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {ToastService , ContactService} from '../../_services'
@Component({
  selector: 'app-add-contact',
  templateUrl: './add-contact.component.html',
  styleUrls: ['./add-contact.component.scss']
})
export class AddContactComponent implements OnInit {
  addForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private toastService: ToastService,
    private contactService:ContactService
  ) { }

  ngOnInit(): void {
    this.addForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      title: ['', Validators.required],
      phone: ['', Validators.required]
    });
  }
  showSuccess() {
    this.toastService.show('Add Contacts successful', { classname: 'bg-success text-light', delay: 3000 });
  }

  showDanger(test) {
    this.toastService.show(test , { classname: 'bg-danger text-light', delay: 3000 });
  }

  // convenience getter for easy access to form fields
  get f() { return this.addForm.controls; }
  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.addForm.invalid) {
      return;
    }
console.log("this.addForm.value"+ JSON.stringify(this.addForm.value))
    this.loading = true;
    this.contactService.addContacts(this.addForm.value)
      
      .subscribe(
        data => {
          this.router.navigate(['/home']);
         this.showSuccess()
        },
        error => {
          this.showDanger(error)
          this.loading = false;
        });
  }


}
