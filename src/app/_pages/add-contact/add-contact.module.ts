import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AddContactRoutingModule } from './add-contact-routing.module';
import { AddContactComponent } from './add-contact.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [AddContactComponent],
  imports: [
    CommonModule,
    AddContactRoutingModule,
    NgbModule,
    ReactiveFormsModule
  ]
})
export class AddContactModule { }
