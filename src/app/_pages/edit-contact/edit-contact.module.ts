import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EditContactRoutingModule } from './edit-contact-routing.module';
import { EditContactComponent } from './edit-contact.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [EditContactComponent],
  imports: [
    CommonModule,
    EditContactRoutingModule,
    NgbModule,
    ReactiveFormsModule
  ]
})
export class EditContactModule { }
