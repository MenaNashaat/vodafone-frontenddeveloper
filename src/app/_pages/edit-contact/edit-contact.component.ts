import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {ToastService , ContactService} from '../../_services';
import { Contact} from '../../_model/contact'
@Component({
  selector: 'app-edit-contact',
  templateUrl: './edit-contact.component.html',
  styleUrls: ['./edit-contact.component.scss']
})
export class EditContactComponent implements OnInit {
  addForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  details: Contact
  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private toastService: ToastService,
    private contactService:ContactService
  ) { }

  ngOnInit(): void {
    this.getDetails()
    this.addForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      title: ['', Validators.required],
      phone: ['', Validators.required]
    });
  }
  showSuccess() {
    this.toastService.show('Update Contact successful', { classname: 'bg-success text-light', delay: 3000 });
  }

  showDanger(test) {
    this.toastService.show(test , { classname: 'bg-danger text-light', delay: 3000 });
  }
  // convenience getter for easy access to form fields
  get f() { return this.addForm.controls; }

  getDetails()
  {
    const id = this.route.snapshot.paramMap.get('id');
    this.contactService.getContactById(id).subscribe(
      data => {
        this.details = data;
        this.addForm.patchValue({
          title: this.details.title,
          phone: this.details.phone,
          email: this.details.email
        });
      },
      error => {
        this.showDanger(error)
        this.loading = false;
      });
  }
  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.addForm.invalid) {
      return;
    }
    this.loading = true;
    const id = this.route.snapshot.paramMap.get('id');
    this.contactService.editContact( id, this.addForm.value)
      
      .subscribe(
        data => {
          this.router.navigate(['/home']);
         this.showSuccess()
        },
        error => {
          this.showDanger(error)
          this.loading = false;
        });
  }

}
